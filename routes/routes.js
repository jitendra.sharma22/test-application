const express = require('express');
const ApiController = require('../controllers/ApiController'); 
const router = express.Router()
router.post('/bulkimport', ApiController.ImportBulkData); 
router.get('/pagination', ApiController.Pagination); 
module.exports = router;