const products = require('../modals/products');
const ImportBulkData = async (req, res, next) => {
    try {
        let batch = products.collection.initializeOrderedBulkOp();
        
        for (let i = 0; i < 1000000; ++i) {
            let myobj =    {
                "name": "Picky Pencil Sharpener",
                "category": "Stationery",
                "description": "Ultra budget",
                "price": parseFloat("0.67"),
                "rating": parseFloat("1.2"),
              };
            batch.insert(myobj);
          }
        
          batch.execute(function(err, result) {
            console.dir(err);
            console.dir(result);
            
            //db.close();
          });  
          
      } catch {
        console.log('Data not inserted');
      }
};

const Pagination = async (req, res)=>{
     // page index
     let start = 0;
     // products per page
     let limit = 10;
     console.log(req.body);
     if (req.body.start !== null || req.body.start !== "undefined") {
        start = parseInt(req.body.start);
    }

    if (req.body.limit !== null || req.body.limit !== "undefined") {
        limit = parseInt(req.body.limit);
    }

    const pageNumber = req.body.page;
    const pageSize = req.body.pagesize;
    const filter = { category: "Testcat" };
    const query = [
        { $match: filter },
        {
          $facet: {
            total: [
              { $group: { _id: null, count: { $sum: 1 } } },
            ],
            edges: [
             
              { $skip: pageSize * (pageNumber - 1) },
              { $limit: 20 },
            ], 
          },
        },
        { 
          $project: {
            total: '$total.count',
            edges: '$edges',
          },
        }];
        
     try {
        console.log('query',query);
        const aggregatedData = await products.aggregate(query);
        console.log('aggregatedData',aggregatedData);
        
        return res.status(200).json({
            data: aggregatedData[0].data,
            totalData: {
                data: aggregatedData,
            }
        });
    } catch (error) {
        return res.status(500).json({
            error: error
        });
    }
}


module.exports = {ImportBulkData,Pagination};