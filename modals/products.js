const mongoose = require('mongoose');

const dataSchema = new mongoose.Schema({
    name: {
        required: true,
        type: String
    },
    category: {
        required: true,
        type: String
    },
    description:{
        type:String
    },
    price:{
        type:Number
    },
    rating:{
        type:Number 
    }
})

module.exports = mongoose.model('Products', dataSchema)