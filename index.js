const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require("body-parser");

require('dotenv').config();
const routes = require('./routes/routes');

//console.log('env',process.env);
const mongoString = process.env.DATABASE_URL


const app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use('/api', routes);
//app.use(express.json());

mongoose.connect(mongoString); 
const database = mongoose.connection

database.on('error', (error) => {
    console.log(error)
})

database.once('connected', () => {
    console.log('Database Connected'); 
})

app.listen(3000, () => {
    console.log(`Server Started at ${3000}`)
})